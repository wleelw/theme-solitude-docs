---
title: 简介
description: 欢迎使用 Hexo 主题 Solitude
---

![master version](https://img.shields.io/github/package-json/v/wleelw/hexo-theme-solitude/master?color=%231ab1ad&label=master){ style="display: inline-block; margin-right: 10px;" }
![master version](https://img.shields.io/github/package-json/v/wleelw/hexo-theme-solitude/dev?label=dev){ style="display: inline-block; margin-right: 10px;" }
![https://img.shields.io/npm/v/hexo-theme-solitude?color=%09%23bf00ff](https://img.shields.io/npm/v/hexo-theme-solitude?color=%09%23bf00ff){ style="display: inline-block; margin-right: 10px;" }
![hexo version](https://img.shields.io/badge/hexo-6.3.0+-0e83c){ style="display: inline-block; margin-right: 10px;" }
![license](https://img.shields.io/github/license/wleelw/hexo-theme-solitude?color=FF5531){ style="display: inline-block;" }

![首页](https://bu.dusays.com/2024/01/29/65b74d4acdb39.png)

主题预览：  [王卓Sco](https://blog.wzsco.top/)  ｜  [亦封](https://blog.meuicat.cn/)  ｜  [Demo](https://solitude.wzsco.top/)

主题交流群：

![QQ群](https://bu.dusays.com/2024/01/29/65b754b8f0f93.png){style="width: 150px;border-radius: 12px;"}

## 🎉 特色

* [x] 页面组件懒加载(pjax方案)
* [x] 图片懒加载
* [x] 多种代码高亮方案
* [x] 多语言配置
* [x] 支持多种评论插件
* [x] 内置网页访问统计
* [x] 支持暗色模式
* [x] 支持脚注语法
* [x] 支持自定义CDN静态资源
* [x] 支持多功能右键菜单
* [x] 支持定制化的主色调随封面图片颜色变化
* [x] 支持沉浸式状态栏
* [x] 支持聊天系统
* [x] 支持百度分析
* [x] 支持图片大图查看
* [x] 支持瀑布流即刻说说
* [x] 支持自定义图标（iconfont、CoDeSign、fontawesome）
* [x] 支持高速缓存的swpp，pwa特性
* [x] 优秀的隐私协议支持
* [x] 文章摘要AI支持
* [x] 支持全局中控台
* [x] 支持本地搜索/algolia搜索🔍
* [x] 支持 Katex 数学公式

## ✨ 贡献者

<a href="https://github.com/wleelw/hexo-theme-solitude/graphs/contributors">
  <img src="https://contrib.rocks/image?repo=wleelw/hexo-theme-solitude" />
</a>

**本主题为 [Heo](https://blog.zhheo.com/) 的衍生主题，由 [@张洪Heo](https://github.com/zhheo) 作者全权授权、设计！**

> 欢迎你对本主题做出贡献！[贡献指南](https://github.com/wleelw/hexo-theme-solitude/blob/main/CONTRIBUTING.md)

## 📷 截图

### 首页

![首页](https://bu.dusays.com/2024/01/29/65b74d4acdb39.png)

### 即刻短文

![即刻短文](https://bu.dusays.com/2024/01/29/65b74e5d5bdf8.png)

### 友情链接

![友情链接](https://bu.dusays.com/2024/01/29/65b74e9a1443f.png)

### 音乐馆

![音乐馆](https://bu.dusays.com/2024/01/29/65b74ede2f1b0.png)

### 装备

![装备](https://bu.dusays.com/2024/01/29/65b74f6f62c42.png)

### 工具箱

![工具箱](https://bu.dusays.com/2024/01/29/65b74fb37c2d9.png)

### 关于

![关于](https://bu.dusays.com/2024/01/29/65b7503bea6a1.png)
