export default defineAppConfig({
  ui: {
    primary: 'blue',
    gray: 'slate',
    header: {
      title: 'Solitude',
    },
    footer: {
      bottom: {
        left: 'text-sm text-gray-500 dark:text-gray-400',
        wrapper: 'border-t border-gray-200 dark:border-gray-800'
      }
    }
  },
  seo: {
    siteName: 'Solitude - 一款优雅的 Hexo 主题',
  },
  header: {
    title: 'Solitude',
    logo: {
      alt: '',
      light: '',
      dark: ''
    },
    search: true,
    colorMode: true,
    links: [{
      icon: 'i-simple-icons-github',
      to: 'https://github.com/wleelw/hexo-theme-solitude',
      target: '_blank',
      'aria-label': 'Theme on GitHub'
    }]
  },
  footer: {
    credits: 'Copyright © 2023 Solitude',
    colorMode: false,
    links: [{
      icon: 'i-simple-icons-bloglovin',
      to: 'https://blog.wzsco.top/',
      target: '_blank',
      'aria-label': '王卓Sco Website'
    }, {
      icon: 'i-simple-icons-github',
      to: 'https://github.com/wleelw/hexo-theme-solitude',
      target: '_blank',
      'aria-label': 'Solitude on GitHub'
    }]
  },
  toc: {
    title: '目录',
    bottom: {
      title: '相关链接',
      edit: 'https://github.com/wleelw/theme-solitude-docs/edit/main/content',
      links: [{
        icon: 'i-heroicons-star',
        label: '点个 Star 吧！',
        to: 'https://github.com/wleelw/hexo-theme-solitude',
        target: '_blank',
      },
        {
          icon: 'i-ri-blogger-fill',
          label: '作者博客',
          to: 'https://blog.wzsco.top/',
        }
      ]
    }
  }
})
