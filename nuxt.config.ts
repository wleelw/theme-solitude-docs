// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  extends: ['@nuxt/ui-pro'],
  modules: [
    '@nuxt/content',
    '@nuxt/ui',
    '@nuxthq/studio',
    '@nuxtjs/fontaine',
    '@nuxtjs/google-fonts',
    'nuxt-og-image'
  ],
  hooks: {
    // Define `@nuxt/ui` components as global to use them in `.md` (feel free to add those you need)
    'components:extend': (components) => {
      const globals = components.filter((c) => ['UButton', 'UIcon'].includes(c.pascalName))

      globals.forEach((c) => c.global = true)
    }
  },
  ui: {
    icons: ['heroicons', 'simple-icons', 'ri']
  },
  app: {
    head: {
      script: [
        {
          'src': '//sdk.51.la/js-sdk-pro.min.js',
          'id': 'LA_COLLECT',
        },
        {
          'innerHTML': `LA.init({id:"3F15j2vtdTo7PPcN",ck:"3F15j2vtdTo7PPcN"})`,
        },
        {
          'innerHTML': `var _hmt = _hmt || [];(function() {  var hm = document.createElement("script");  hm.src = "https://hm.baidu.com/hm.js?9e659c3023eaa533e68f1e56c4f0f329";var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(hm, s);})();`,
        }
      ]
    }
  },
  // Fonts
  fontMetrics: {
    fonts: ['DM Sans']
  },
  googleFonts: {
    display: 'swap',
    download: true,
    families: {
      'DM+Sans': [400, 500, 600, 700]
    }
  },
  routeRules: {
    '/api/search.json': {prerender: true},
  },
  devtools: {
    enabled: true
  },
  typescript: {
    strict: false
  },
})
